#!/usr/bin/env python2.7
# -*- coding: utf-8 -*-
"""
Central place to store parameters / settings / variables.
All length, height etc. parameters are in meters, square meters (m2) etc.

The assigned values are default values. The config file will overwrite them.

Created on May 27, 2013

@author: vanosten

Ludomotico contributed a cleaner version of read_from_file().
"""

import argparse
import sys
import types
from os.path import os
from pdb import pm
import logging
import traceback

# default_args_start # DO NOT MODIFY THIS LINE
# -*- coding: utf-8 -*-
# The preceeding line sets encoding of this file to utf-8. Needed for non-ascii 
# object names. It must stay on the first or second line.

LOGLEVEL = "INFO"
quiet = False

# -- Scenery folder, typically a geographic name or the ICAO code of the airport
# -- Full path to the scenery folder without trailing slash. This is where we
#    will probe elevation and check for overlap with static objects. Most
#    likely you'll want to use your TerraSync path here.
SCENERY_PATH = "/home/user/fgfs/scenery/TerraSync"

INPUT_PATH = ""
# -- The generated scenery (.stg, .ac, .xml) will be written to this path.
#    If empty, we'll use the correct location in PATH_TO_SCENERY. Note that
#    if you use TerraSync for PATH_TO_SCENERY, you MUST choose a different
#    path here. Otherwise, TerraSync will overwrite the generated scenery.
#    Also make sure PATH_TO_OUTPUT is included in your $FG_SCENERY.
OUTPUT_PATH = "/home/user/fgfs/scenery/converted"

NO_ELEV = False             # -- skip elevation probing
FG_ELEV = '"D:/Program Files/FlightGear/bin/Win64/fgelev.exe"'

NO_BLENDER = False
FLIP_DDS = True
CONVERT_DDS_TO_PNG = False

# -- Path to nvdxt.exe, part of the NVidia DDS utilities
NVDXT = "/home/tom/.wine/drive_c/Programme/DDS_Utilities/nvdxt.exe"
NVDXT_OPTIONS = "" # -- further options for nvDXT, e.g. to enable a certain compression

# default_args_end # DO NOT MODIFY THIS LINE

def show():
    """
    Prints all parameters as key = value
    """
    global quiet
    if quiet: return
    print '--- Using the following parameters: ---'
    my_globals = globals()
    for k in sorted(my_globals.iterkeys()):
        if k.startswith('__'):
            continue
        elif k == "args":
            continue
        elif k == "parser":
            continue
        elif isinstance(my_globals[k], types.ClassType) or \
                isinstance(my_globals[k], types.FunctionType) or \
                isinstance(my_globals[k], types.ModuleType):
            continue
        else:
            print '%s = %s' % (k, my_globals[k])
    print '------'


def read_from_file(filename):
    logging.info('Reading parameters from file: %s' % filename)
    default_globals = globals()
    file_globals = {'textures' : default_globals['textures']}
    try:
        execfile(filename, file_globals)
    except IOError, reason:
        logging.error("Error processing file with parameters: %s", reason)
        sys.exit(1)
    except NameError:
        print traceback.format_exc()
        logging.error("Error while reading " + filename + ". Perhaps an unquoted string in your parameters file?")
        sys.exit(1)
        
    for k, v in file_globals.iteritems():
        if k.startswith('_'): 
            continue
        k = k.upper()
        if k in default_globals:
            default_globals[k] = v
        else:
            logging.warn('Unknown parameter: %s=%s' % (k, v))


def show_default():
    """show default parameters by printing all params defined above between
        # default_args_start and # default_args_end to screen.
    """
    f = open(sys.argv[0], 'r')
    do_print = False
    for line in f.readlines():
        if line.startswith('# default_args_start'):
            do_print = True
            continue
        elif line.startswith('# default_args_end'):
            return
        if do_print:
            print line,
            
def set_loglevel(args_loglevel = None):
    """set loglevel from paramters or command line"""
    global LOGLEVEL
    if args_loglevel != None:
        LOGLEVEL = args_loglevel
    LOGLEVEL = LOGLEVEL.upper()
    # -- add another log level VERBOSE. Use this when logging stuff in loops, e.g. per-OSM-object,
    #    potentiallially flooding the screen
    logging.VERBOSE = 5
    logging.addLevelName(logging.VERBOSE, "VERBOSE")
    logging.Logger.verbose = lambda inst, msg, *args, **kwargs: inst.log(logging.VERBOSE, msg, *args, **kwargs)
    logging.verbose = lambda msg, *args, **kwargs: logging.log(logging.VERBOSE, msg, *args, **kwargs)

    numeric_level = getattr(logging, LOGLEVEL, None)
    if not isinstance(numeric_level, int):
        raise ValueError('Invalid log level: %s' % LOGLEVEL)
    logging.basicConfig(level=numeric_level)
    logging.getLogger().setLevel(LOGLEVEL)
    
    global quiet
    if numeric_level > logging.INFO:
        quiet = True

if __name__ == "__main__":
    # Handling arguments and parameters
    parser = argparse.ArgumentParser(
        description="Library for parsing a parameters file - used as main it shows the parameters used.")
    parser.add_argument("-f", "--file", dest="filename",
                        help="read parameters from FILE (e.g. params.ini)", metavar="FILE")
    parser.add_argument("-d", "--show-default", action="store_true", help="show default parameters")
    args = parser.parse_args()
    if args.filename is not None:
        read_from_file(args.filename)
        show()
    if args.show_default:
        show_default()
